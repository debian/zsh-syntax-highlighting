Source: zsh-syntax-highlighting
Section: shells
Priority: optional
Maintainer: Debian Zsh Maintainers <pkg-zsh-devel@lists.alioth.debian.org>
Uploaders: Daniel Shahaf <danielsh@apache.org>
Build-Depends:
	debhelper-compat (= 13),
	zsh,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/zsh-syntax-highlighting
Vcs-Git: https://salsa.debian.org/debian/zsh-syntax-highlighting.git
Homepage: https://github.com/zsh-users/zsh-syntax-highlighting/
Rules-Requires-Root: no

Package: zsh-syntax-highlighting
Architecture: all
Depends: zsh, ${misc:Depends}
Enhances: zsh
Description: Fish shell like syntax highlighting for zsh
 This package provides syntax highlighting for the shell zsh.  It enables
 highlighting of commands whilst they are typed at a zsh prompt into an
 interactive terminal.  This helps in reviewing commands before running
 them, particularly in catching syntax errors.
 .
 This feature is inspired by the Fish shell, which provides it by default.
 .
 There are numerous ways to configure the colors used for highlighting,
 and to configure what is highlighted.
